import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from 'react-router-dom';
import "rsuite/dist/styles/rsuite-default.css";
import {Provider} from "react-redux"
import store from './redux/store';
import { IntlProvider } from 'rsuite';
import arEG from 'rsuite/lib/IntlProvider/locales/ar_EG';

ReactDOM.render(
  <Provider store={store}>
    <IntlProvider locale={arEG} rtl>
      <React.StrictMode>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </React.StrictMode>
    </IntlProvider>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
