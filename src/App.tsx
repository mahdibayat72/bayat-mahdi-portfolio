import { useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import HeaderLayout from './layouts/header.layout';
import FooterLayout from './layouts/footer.layout';
import HomePage from './pages/home.page';
import { connect } from 'react-redux';
import { ModeSelector, DirSelector, DirectionSelector } from "./redux/selector";
import { createStructuredSelector } from 'reselect';

//@ts-ignore
const App = ({Lng : string, Direction: string, Mode: string}) => {

  let src = "/globalStyle/rsuite-default.css";

    useEffect(()=>{
        switch (Mode) {
            case "light":
                if(Direction === "rtl"){
                    src = "/globalStyle/rsuite-default-rtl.css"
                }else{
                    src = "/globalStyle/rsuite-default.css"
                }
                break;
            case "dark":
                if(Direction === "ltr"){
                    src = "/globalStyle/rsuite-dark.css"
                }else{
                    src = "/globalStyle/rsuite-dark-rtl.css"
                }
                break;
            default:
                break;
        }
        document.getElementById('MainStyle').href = src;
    },[Mode]);

  return (
    <div id="Style">
      <HeaderLayout />
      <Switch>
        <Route path="/" component={HomePage} />
      </Switch>
      <FooterLayout />
    </div>
  );
}

const mapStateToProps= createStructuredSelector({
  Mode: ModeSelector,
  Direction: DirectionSelector,
  Lng: DirSelector
})

export default connect(mapStateToProps)(App);
