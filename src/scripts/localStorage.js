
export default class LocalStorage {// لایه برای کار کردن با local storage.
    static setItem(key, value) {
        window.localStorage.removeItem(key);
        return window.localStorage.setItem(key, value);
    }

    static getItem(key) {
        return window.localStorage.getItem(key);
    }

    static removeItem(...keys) {
        for(const key of keys) {
            window.localStorage.removeItem(key);
        }
    }

    static clear() {
        return window.localStorage.clear();
    }
}