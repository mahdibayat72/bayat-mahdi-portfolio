import axios from 'axios';
import { useCookies, Cookies } from "react-cookie";
import { Alert } from 'rsuite';
import { Constants } from './helper';

const _cookies = new Cookies();

const handleError = (response) => {
    let data = response['data'];
    let status = response['status'];
    console.log("ERROR LOGS - NEED HANDLE ERROR");
    console.log(response);
    if(status === 500) {
        return Alert.info(data.message, 4000)
    }
    // if(status === 422) {
    //     return Object.values(data.errors).forEach(item => Alert.error(`${item}`));
    // }
    // if(status === 401) {
    //     _cookies.remove('AccessToken', { path: '/', domain: window.location.hostname });
    //     _cookies.remove('mobile', { path: '/', domain: window.location.hostname });
    //     _cookies.remove('user', { path: '/', domain: window.location.hostname });
    //     router.push('/account');
    // }
    // if(status === 400) {
    //     _cookies.remove('AccessToken', { path: '/', domain: window.location.hostname });
    //     _cookies.remove('mobile', { path: '/', domain: window.location.hostname });
    //     _cookies.remove('user', { path: '/', domain: window.location.hostname });
    //     router.push('/account');
    //     Alert.error("کد وارد شده نامعتبر است")
    // }
};

export const get = (url) => {
    axios.defaults.baseURL = Constants.baseUrl;
    // axios.defaults.headers.common['Authorization'] = `Bearer ${_cookies.get('AccessToken') || ''}`;
    return new Promise((resolve, reject) => {
        axios.get(url)
            .then((response) => {
                if (response && response.data) {
                    resolve(response.data);
                } else {
                    handleError(response , response.statusCode)
                }
            })
            .catch((error) => {
                handleError(error.response);
                reject(error && error.response && error.response.data);
            });
    });
};

const post = (url, params = {}, skip) => {
    axios.defaults.baseURL = Constants.baseUrl2;
    return new Promise((resolve, reject) => {
        axios.post(url, params, {headers : {
                'Content-Type': 'application/json',
                "accept": "*/*"
            }})
            .then((response) => {
                resolve(response.data)
            })
            .catch((error) => {
                handleError(error.response)
                reject(error && error.response && error.response.data)
            });
    });
};

const put = (url, params = {}) => {
    axios.defaults.baseURL = Constants.baseUrl;
    axios.defaults.headers.common['Authorization'] = `Bearer ${_cookies.get('AccessToken') || ''}`;
    return new Promise((resolve, reject) => {
        axios.put(url, params)
            .then((response) => {
                if (response && response.data) {
                    if (response.data.status === 1) {
                        resolve(response.data);
                    } else {
                        handleError(response);
                        reject(response.data);
                    }
                }
            })
            .catch((error) => {
                handleError(error && error.response && error.response.data);
                reject(error && error.response && error.response.data);
            });
    });
};

const del = (url, params = {}) => {
    axios.defaults.baseURL = Constants.baseUrl;
    axios.defaults.headers.common['Authorization'] = `Bearer ${_cookies.get('AccessToken') || ''}`;
    return new Promise((resolve, reject) => {
        axios.delete(url, params)
            .then((response) => {
                if (response && response.data) {
                    if (response.data.status === 1) {
                        resolve(response);
                    } else {
                        handleError(response.data);
                        reject(response.data);
                    }
                }
            })
            .catch((error) => {
                handleError(error && error.response && error.response.data);
                reject(error && error.response && error.response.data);
            });
    });
};

export default {
    coin: {
        tokens: (params = {network: 1, qs: ""}) => {
            return get(`${params.network}/tokens`);
        },
        quote: (params = {}) => {
            return get(`${params.network}/quote/?${params.qs}`)
        },
        swap: (params = {}) => {
            return get(`${params.network}/swap/?${params.qs}`)
        },
        approve: {
            callData : (params = {network: 1, qs: ""}) => {
                return get(`${params.network}/approve/calldata/?${params.qs}`);
            },
            spender : (params = {network: 1, qs: ""}) => {
                return get(`${params.network}/approve/spender`);
            }
        },
        price: (params = {}) => {
            return get(`https://token-prices.1inch.io/v1.1/${params.Network}`)
        }
    },
    limit: {
        request: (params = {}) => {
            return post("/1/limit-order", params)
        }
    },
    coingecko : {
        price: (params = {}) => {
            return get(`https://api.coingecko.com/api/v3/simple/token_price/${params.Network}?contract_addresses=${params.address}&vs_currencies=usd`)
        }
    }
};
