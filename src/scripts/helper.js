
const Constants = {
    baseUrl: 'https://api.1inch.exchange/v3.0/',
    baseUrl2: 'https://limit-orders.1inch.exchange/v1.0/',
    dateFormat: 'YYYY-MM-DD',
    datetimeFormat: 'YYYY-MM-DD HH:mm',
    expireToken: 7,
    siteUrl: 'https://arix.exchange/'
};

const Patterns = {
    username: /^[A-Za-z0-9]([A-Za-z0-9_]{1,48})[A-Za-z0-9_]$/,
    urlname: /^[A-Za-z]([A-Za-z0-9-]{1,48})[A-Za-z0-9-]$/,
    mobile: /^(09)[0-9]{9}$/,
    email: /^\w+([-.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
    name: /^[A-Za-z\u0600-\u06FF\u200c][A-Za-z\u0600-\u06FF\u200c ]{1,48}[A-Za-z\u0600-\u06FF\u200c]$/,
    rolename: /^[A-Za-z0-9\u0600-\u06FF\u200c][0-9A-Za-z\u0600-\u06FF\u200c ]{1,48}[A-Za-z0-9\u0600-\u06FF\u200c]$/,
    latlong: /^[0-9]{2,3}(\.)[0-9]{6}$/,
    tag: /^[a-zA-Z0-9_\u0600-\u06FF\u200c]{2,20}$/
};

const WalletAddressSimplefy = (Address) => {
    let newAddress;
    newAddress = Address.split("").filter((x,i)=> i < 10).join("");
    return newAddress + " ..."
};

export { Constants, Patterns, WalletAddressSimplefy };
