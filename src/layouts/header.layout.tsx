import styled from 'styled-components';
import {Affix} from 'rsuite';

const HeaderContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  box-shadow: 0px 1px 7px silver;
`

const HeaderLayout = () => {
  return (
    <Affix top={0}>
      <HeaderContainer>
        <h1>HeaderLayout</h1>
      </HeaderContainer>
    </Affix>
  );
};

export default HeaderLayout;