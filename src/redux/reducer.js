import { combineReducers } from 'redux';
import {siteTypes, userTypes} from './types';

// SITE REDUCER
const SITE_INITIAL_STATE = {
  mode: "DARK",
  lng: "FA",
}
const siteReducer = (state = SITE_INITIAL_STATE , action) => {
  switch (action.type) {
    case siteTypes.CHANGE_MODE :
      return {...state, mode: action.payload};
    default :
      return state;
  }
}

// USER REDUCER
const USER_INITIAL_STATE = {
  user: null
}
const userReducer = (state = USER_INITIAL_STATE, action) => {
  switch (action.payload) {
    case userTypes.SET_USER:
      return {...state, user : action.payload}
    default : 
      return state;
  }
}

export const rootReducer = combineReducers({
  site : siteReducer,
  user : userReducer
})