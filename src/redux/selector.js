import { createSelector } from "reselect";

const SiteSelector = state => state.site;

export const ModeSelector = createSelector(
  [SiteSelector],
  site => site.mode
);
export const DirectionSelector = createSelector(
  [SiteSelector],
  site => site.direction
);
export const DirSelector = createSelector(
  [SiteSelector],
  site => site.dir
);