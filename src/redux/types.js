
// SITE TYPES
export const siteTypes = {
  SET_MODE : "SET_MODE",
  SET_DIRECTION : "SET_DIRECTION",
  SET_DIR : "SET_DIR"
}

// USER TYPES
export const userTypes = {
  SET_USER : "SET_USER"
}