import { siteTypes } from "./types";

// SITE ACTIONS

export const ChangeMode = (mode) => ({
  type : siteTypes.CHANGE_MODE,
  payload : mode
})