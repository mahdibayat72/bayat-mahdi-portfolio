import React from 'react';
import { Button } from 'rsuite';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { SelectMode } from '../redux/selector';
import { ChangeMode } from '../redux/actions';

const HomePage = (ChangeMode: any, Mode: string) => {
  return (
    <div>
      {console.log(Mode)}
      <h1>HOmePage</h1>
      <Button size="lg" onClick={()=>ChangeMode("Light")} >Change Mode</Button>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  Mode : SelectMode
})
const mapDispatchToProps = (dispatch : any) => ({
  ChangeMode : (mode: string) => dispatch(ChangeMode(mode))
})

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);